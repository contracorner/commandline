README
======

This is a command line argument parser. 

The key feature is the settings (aka switches or options) 
are defined using C#'s attribute syntax on normal class fields.
The processor handles the conversion of raw command line text into data type specific values.

Sample
------

Here is a sample program whose usage would be:

    test.exe [/verbose] /message="_TEXT_" [/count=_NUMBER_]

For example:

    test.exe /message="Hello, dear World." /count=3

The C# code is:

    class Program
    {
        class MyCommandLine : StandardCommandLine
        {
            [Setting(Required = true)]
            public string Message = null;

            [Setting]
            public int Count = 1;
        }

        static void Main(string[] args)
        {
            var p = new Program();
            p.Run(args);
        }

        int Run(string[] args)
        {
            MyCommandLine cl = new MyCommandLine();
            //cl.Prefix = "-";
            //cl.Separator = ":";
            CommandLine.Status status = cl.Process(args);
            if (status == CommandLine.Status.ERROR) return -1;
            if (status == CommandLine.Status.HELP) return 0;
 
            if (cl.Verbose)
                Console.WriteLine($"Count is {cl.Count}");
            for (int i = 0; i < cl.Count; ++i)
            {
                Console.WriteLine(cl.Message);
            }
            return 0;
        }
    }

Defining the command line
=========================

A command line is a collection of settings and possibly "unnamed" arguments.
You define your settings as a set of fields in a custom subclass of `CommandLine` or `StandardCommandLine`. 
Each setting field is annotated with a [Setting] attribute so it is known to be a named setting.
Unnamed arguments are simply collected in the field `UnnamedArgs`.

The `StandardCommandLine` subclass extends the root class by adding a few commonly useful settings. 
If you don't want them then use the `CommandLine` root class which doesn't provide any
default settings.

The field's name can be any letter case because the default string comparer is
case-insensitive.

The field's type should be something directly useful to your tool.
All of the simple .NET types are convertible so you get those string conversions for free.
If you use your own custom types you must implement a [TypeConverter](https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.typeconverter) for it.
The field's value is its default type.
Required fields should be initialize null to avoid unwanted compiler warnings.

These are the properties you can add to the Setting attribute:

**`Usage`** — a terse description of the setting. See "Usage method" below.

**`Required`** — true if this setting must exist on the command line.

**`Special`** — true if this setting is special and used standalone. It disables checking of all other required settings.

CommandLine methods
===================
Your main program will typically instantiate your custom CommandLine object, 
and then call `Process()` while passing the raw args array received from the operating system.
After checking the return status is OK, then you can access the fields as you like to perform
your tool's functions.

CommandLine contructor
----------------------
     public CommandLine(StringComparer keyComparer = null)
Typically you construction your instance with no arguments.
That will create a case-insensitive instance which allows your users to capitalize setting names
as they wish.
You can change that to case-sensitive by passing `keyComparer: StringComparer.InvariantCulture` to the base class in your subclass contructor:
    public class MyCommandLine : StandardCommandLine
    {
        //...
        public MyCommandLine() 
            : base(keyComparer: StringComparer.InvariantCulture)
        { }
    }

Process method
--------------
    public Status Process(params string[] args)
Pass in the raw command line string array.
The result will be one of the `CommandLine.Status` values:

* OK — The raw arguments were successfully processed into your setting fields
* HELP — The `StandardCommandLine.Help` setting was used.
* ERROR — An error was found and reported to your user on the Console. Processing of the raw arguments is not complete and your setting fields are not in a known state.

When this method returns OK you can access your setting fields and the special `UnnamedArgs` list.

PostProcess method
------------------
    virtual public Status PostProcess()
The root class implementation returns OK.
You can _extend_ this to add additional logic.
While you can do further processing or error-checking after calling Process, 
the advantage of using PostProcess is it keeps that logic with your settings fields.
Without using it, if you use your class is several places (especially in automated testing) 
you'd have to copy and paste that logic everywhere.

Be sure to extend the method by calling the `base.PostProcess()` method before or after your own
processing.


Usage method
------------
    public void Usage(Action<string, bool, string, object, string> write = null)
This displays the Usage strings of all settings. 
It accepts an optional Action that takes these parameters:
* string fieldName - The setting (field) name
* bool required - The Setting's `Required` value.
* string typeName - The full name of the data type. (System types have the leading "System." removed.)
* object defaultValue - The property default value
* string usage - The Setting's `Usage` value.
If you omit the `Action` argument then these data are formatted and written to Console for you.

StandardCommandLine
===================
This subclass provides standard definitions for `Verbose` and `Help`.
`Help` is marked as Special, so it can be used alone on the command line.
Because of `Help` this class extends `PostProcess()`. 
If the user used the Help setting the this calls Usage() and returns status HELP.

Credit, License, and Maintenance
================================

* Author: Bob Peterson
* Mail: peterson.at.site.contracorner.com

See LICENSE.txt for the license terms.