using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CCToolbox;
using System.Collections.Generic;

namespace SettingsTests
{

    public class TestCommandLine : StandardCommandLine
    {
        [Setting(Usage = "A greeting message")]
        public string Message = "Hello World!";

        [Setting(Usage = "A significant event")]
        public DateTime When = DateTime.Parse("Feb 10, 1956");

        [Setting]
        public int Count = 42;

        [Setting]
        public float Narrow = 1.0F;

        [Setting]
        public double Wide = 2.0;
    }

    public class TestCommandLine2 : StandardCommandLine
    {
        [Setting(Required=true)]
        public string Message1;
        [Setting(Required = true)]
        public string Message2;
        [Setting(Required = true)]
        public string Message3;
    }

    public class TestCommandLine3 : StandardCommandLine
    {
        [Setting(Required = true, Usage = "A greeting message")]
        public string Message = "Hello World!";

        [Setting(Usage = "A significant event")]
        public DateTime When = DateTime.Parse("Feb 10, 1956");

        [Setting]
        public int Count = 42;
    }

    public class TestCommandLine4 : StandardCommandLine
    {
        [Setting]
        public int verbose = 3;

        public TestCommandLine4(StringComparer keyComparer = null) : base(keyComparer: keyComparer) { }
    }

    [TestClass]
    public class CommandLineTests
    {
        #region Bool /verbose
        [TestMethod]
        public void TestVerbose()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process("/verbose");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
        }
        [TestMethod]
        public void TestVerbose_default()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process();
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsFalse(cl.Verbose);
        }
        [TestMethod]
        public void TestVerbose_True()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process("/verbose= true"); // also testing space-trimming
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
        }
        [TestMethod]
        public void TestVerbose_T()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process("/verbose=T");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
        }
        [TestMethod]
        public void TestVerbose_Yes()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process("/verbose=Yes");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
        }
        [TestMethod]
        public void TestVerbose_y()
        {
            StandardCommandLine cl = new StandardCommandLine();
            var status = cl.Process("/verbose=y");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
        }
        #endregion

        #region String
        [TestMethod]
        public void TestString_default()
        {
            var cl = new TestCommandLine();
            var status = cl.Process();
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual("Hello World!", cl.Message);
        }

        [TestMethod]
        public void TestString()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/message=truth=justice");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual("truth=justice", cl.Message);
        }

        [TestMethod]
        public void TestString_missing()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/message");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual("", cl.Message);
        }

        [TestMethod]
        public void TestString_empty()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/message=");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual("", cl.Message);
        }
        #endregion

        #region Int
        [TestMethod]
        public void TestInt()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/count=11");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual(11, cl.Count);
        }
        #endregion

        #region Float
        [TestMethod]
        public void TestFloat()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/narrow=3.14159");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual(3.14159F, cl.Narrow);
        }
        #endregion

        #region Double
        [TestMethod]
        public void TestDouble()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/wide=6.02214076E23");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual(6.02214076E23, cl.Wide);
        }
        #endregion

        #region Date
        [TestMethod]
        public void TestDate()
        {
            var cl = new TestCommandLine();
            var status = cl.Process("/When = feb 11, 1956");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual(DateTime.Parse("Feb 11, 1956"), cl.When);
        }
        #endregion

        #region Required
        [TestMethod]
        public void TestRequired_missing3()
        {
            var cl = new TestCommandLine2();
            var status = cl.Process();
            Assert.AreEqual(CommandLine.Status.ERROR, status);
        }

        [TestMethod]
        public void TestRequired_missing2()
        {
            var cl = new TestCommandLine2();
            var status = cl.Process("/message1=truth=justice");
            Assert.AreEqual(CommandLine.Status.ERROR, status);
        }

        [TestMethod]
        public void TestRequired_missing1()
        {
            var cl = new TestCommandLine2();
            var status = cl.Process(
                "/message1=truth=justice",
                "/message2=fidelity≠loyalty");
            Assert.AreEqual(CommandLine.Status.ERROR, status);
        }

        [TestMethod]
        public void TestRequired_present3()
        {
            var cl = new TestCommandLine2();
            var status = cl.Process(
                "/message1=truth=justice",
                "/message2=fidelity≠loyalty",
                "/message3=honesty>transparency");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.AreEqual("truth=justice", cl.Message1);
        }
        #endregion

        #region Usage

        [TestMethod]
        public void TestUsage_content()
        {
            var cl = new TestCommandLine3();
            List<string> actual = new List<string>();
            cl.Usage((name,required,type,dv,line) => actual.Add($"{name} {required} {type} {dv} {line}"));
            actual.ForEach(l => Console.WriteLine(l));
            Assert.AreEqual(5, actual.Count);
            int i = 0;
            Assert.AreEqual("Count False Int32 42 ", actual[i++]);
            Assert.AreEqual("Help False Boolean False List possible settings", actual[i++]);
            Assert.AreEqual("Message True String Hello World! A greeting message", actual[i++]);
            Assert.AreEqual("Verbose False Boolean False Enable verbose output", actual[i++]);
            Assert.AreEqual("When False DateTime 02/10/1956 00:00:00 A significant event", actual[i++]);
        }

        [TestMethod]
        public void TestUsage_write()
        {
            var cl = new TestCommandLine3();
            List<string> actual = new List<string>();
            cl.Usage();
            // This test result must be examined by hand
        }

        #endregion

        #region Usage

        [TestMethod]
        [ExpectedException(typeof(CommandLine.DuplicateSetting))]
        public void TestDuplicateSetting_caseless()
        {
            new TestCommandLine4();
        }

        [TestMethod]
        public void TestDuplicateSetting_caseSensitive()
        {
            var cl = new TestCommandLine4(keyComparer: StringComparer.InvariantCulture);
            var status = cl.Process("/Verbose=true");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsTrue(cl.Verbose);
            Assert.AreEqual(3, cl.verbose);

            cl = new TestCommandLine4(keyComparer: StringComparer.InvariantCulture);
            status = cl.Process("/verbose = 5");
            Assert.AreEqual(CommandLine.Status.OK, status);
            Assert.IsFalse(cl.Verbose);
            Assert.AreEqual(5, cl.verbose);
        }

        #endregion
    }
}
