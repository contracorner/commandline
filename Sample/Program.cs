﻿using System;
using CCToolbox;

namespace Sample
{
    class Program
    {
        class MyCommandLine : StandardCommandLine
        {
            [Setting(Required = true)]
            public string Message = null;

            [Setting]
            public int Count = 1;
        }

        static void Main(string[] args)
        {
            var p = new Program();
            p.Run(args);
        }

        int Run(string[] args)
        {
            MyCommandLine cl = new MyCommandLine();
            //cl.Prefix = "-";
            //cl.Separator = ":";
            CommandLine.Status status = cl.Process(args);
            if (status == CommandLine.Status.ERROR)
            {
                return -1;
            }
            if (status == CommandLine.Status.HELP)
            {
                return 0;
            }

            if (cl.Verbose)
                Console.WriteLine($"Count is {cl.Count}");
            for (int i = 0; i < cl.Count; ++i)
            {
                Console.WriteLine(cl.Message);
            }
            return 0;
        }
    }
}
