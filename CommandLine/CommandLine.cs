﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace CCToolbox
{
    /// <summary>
    /// All command line processing starts from this base case.
    /// Define your own custom command line by extending this class or one of the standard subclasses provided.
    /// Declare fields with [Setting] attributes to define the settings ("switches" or "options" are other common terms) of your command line.
    /// </summary>
    public abstract class CommandLine
    {
        public enum Status
        {
            OK,
            HELP,
            ERROR
        }

        public class DuplicateSetting : Exception
        {
            public string Name1, Name2;

            public DuplicateSetting(string a, string b)
            {
                Name1 = a;
                Name2 = b;
            }
            public override string Message { get { return $"Duplicate Setting names \"{Name1}\" and \"{Name2}\""; } }
        }

        /// <summary>
        /// The settings prefix when initializing the instance.
        /// Typically this is a slash, dash, or double-dash.
        /// The default is "/".
        /// </summary>
        public string Prefix = "/";

        /// <summary>
        /// The separator between the setting name and the value.
        /// The default is "=".
        /// </summary>
        public string Separator = "=";

        /// <summary>
        /// By changing this you can change whether your command line settings names are case insensitive or not.
        /// This is the comparer for testing the actual command line argument name with to setting field names.
        /// The default is .InvariantCultureIgnoreCase.
        /// </summary>
        private StringComparer KeyComparer;

        /// <summary>
        /// Arguments which aren't settings (that is, they don't begin with Prefix) are collected here.
        /// </summary>
        public List<string> UnnamedArgs = new();

        /// <summary>
        /// All fields marked with [Setting].
        /// </summary>
        private Dictionary<string, FieldInfo> SettingFields;

        /// <summary>
        /// Names of required settings.
        /// </summary>
        private IEnumerable<string> RequiredSettingNames;

        /// <summary>
        /// This list tracks all properties that have been used on the command line.
        /// </summary>
        private List<string> EncounteredSettingNames = new();

        private bool ProcessingHasBeenRunOnce = false;

        /// <summary>
        /// Initialize your command line.
        /// </summary>
        public CommandLine(StringComparer keyComparer = null)
        {
            KeyComparer = keyComparer ?? StringComparer.InvariantCultureIgnoreCase;

            // Collect all of the setting fields defined by the all of the subclasses.
            SettingFields = IntrospectForSettingFields();

            RequiredSettingNames = SettingFields.Where(kv =>
            {
                SettingAttribute attr = SettingAttr(kv.Value);
                return attr != null && attr.Required;
            })
                .Select(kv => kv.Value.Name);
        }

        /// <summary>
        /// Process the raw command line argument strings into your settings.
        /// This calls Validate() at the very end to perform the final validation.
        /// </summary>
        /// <param name="args">Raw command line argument strings</param>
        /// <returns>
        /// A Status.
        /// Error handling is simplistic: the first error terminates processing.
        /// </returns>
        public Status Process(params string[] args)
        {
            // You can only call Process once in this object's life cycle.
            if (ProcessingHasBeenRunOnce)
            {
                ErrorCode.Processed.Report("");
                return Status.ERROR;
            }

            var anySpecial = false;

            // Process the raw command line.
            //
            foreach (var arg in args)
            {
                if (arg.StartsWith(Prefix))
                {
                    // Argument begins with the settings prefix.
                    //
                    var parsed = ParseArg(arg);
                    if (SettingFields.ContainsKey(parsed.name))
                    {
                        // A setting has been found for the given name.
                        //
                        // Set the field to the given value (after type-specific parsing).
                        //
                        FieldInfo field = SettingFields[parsed.name];
                        var error = SetField(field, parsed.value);
                        if (error.HasValue)
                        {
                            // The value had some kind of error.
                            error.Value.Report(arg);
                            return Status.ERROR;
                        }
                        // Successful processing of the setting.
                        EncounteredSettingNames.Add(field.Name);
                        anySpecial |= SettingAttr(field).Special;
                    }
                    else
                    {
                        // Argument name is unknown.
                        //
                        ErrorCode.Unknown.Report(arg);
                        return Status.ERROR;
                    }
                }
                else
                {
                    // Argument is unnamed; it doesn't begin with the prefix.
                    //
                    UnnamedArgs.Append(arg);
                }
            }

            // Make sure all required settings have been provided.
            //
            if (!anySpecial)
            {
                IEnumerable<string> missingFields = RequiredSettingNames.Except(EncounteredSettingNames);
                if (missingFields.Any())
                {
                    ErrorCode.RequiredSettings.Report(string.Join(", ", missingFields));
                    return Status.ERROR;
                }
            }

            return PostProcess();
        }

        /// <summary>
        /// Perform final actions after the raw command line values have been processed.
        /// This is for you to extend if you wish.
        /// </summary>
        /// <returns>true if the command line is ready for the main program to use, false if not.
        /// (False does not mean "error" as it could include help processing.)</returns>
        virtual public Status PostProcess()
        {
            return Status.OK;
        }

        /// <summary>
        /// Display the usage values for all setting fields.
        /// </summary>
        /// <param name="write">Optional method to display each field.
        /// The arguments are: (string fieldName, bool required, string typeName, object defaultValue, string usage).</param>
        public void Usage(Action<string, bool, string, object, string> write = null)
        {
            foreach (var pair in SettingFields.OrderBy(kv => kv.Key))
            {
                var attr = SettingAttr(pair.Value);
                var type = pair.Value.FieldType.FullName;
                if (type.StartsWith("System.")) type = type.Substring(7);

                if (write == null)
                {
                    if (attr.Required)
                    {
                        Console.WriteLine($"{Prefix}{pair.Value.Name}{Separator}{type} - {attr.Usage} [Required]");
                    }
                    else if (type == "Boolean")
                    {
                        Console.WriteLine($"{Prefix}{pair.Value.Name}{Separator}{type} - {attr.Usage}");
                    }
                    else
                    {
                        var defaultValue = pair.Value.GetValue(this);
                        var defaultText = defaultValue == null ? "<null>" : defaultValue.ToString();
                        Console.WriteLine($"{Prefix}{pair.Value.Name}{Separator}{type} - {attr.Usage} [Optional, default is {defaultText}]");
                    }
                }
                else
                {
                    var defaultValue = pair.Value.GetValue(this);
                    write(pair.Value.Name, attr.Required, type, defaultValue, attr.Usage);
                }
            }
        }

        /// <summary>
        /// Get the FieldInfo's Setting attribute.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private static SettingAttribute SettingAttr(FieldInfo field)
        {
            return field.GetCustomAttribute<SettingAttribute>();
        }

        /// <summary>
        /// Discover all instance fields.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, FieldInfo> IntrospectForSettingFields()
        {
            var caseInsensitiveFields = new Dictionary<string, FieldInfo>(KeyComparer);

            // Get all instance (non-static) fields, whether public or not.
            // Keep just the fields that have the [Setting] attribute.
            IEnumerable<FieldInfo> fields = this.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Where(f => null != SettingAttr(f));

            // Not cool to use LINQ's .ToDictionary as it deletes the dictionary instantiated above.
            // The we want a dictionary that obeys the configured StringComparer for keys.
            // Doing it as follows preserves that correctly-created instance.
            foreach (var f in fields)
            {
                if (caseInsensitiveFields.ContainsKey(f.Name))
                {
                    throw new DuplicateSetting(f.Name, caseInsensitiveFields[f.Name].Name);
                }
                caseInsensitiveFields[f.Name] = f;
            }

            return caseInsensitiveFields;
        }

        /// <summary>
        /// Parse out the raw setting text.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns>(name,value) tuple.
        /// Name is trimmed, value is not.
        /// Neither case is changed.</returns>
        private (string name, string value) ParseArg(string arg)
        {
            arg = arg.Substring(Prefix.Length);
            var iEquals = arg.IndexOf(Separator);
            if (iEquals == -1)
            {
                return (arg, null);
            }
            else
            {
                return (name: arg.Substring(0, iEquals).Trim(),
                        value: arg.Substring(iEquals + 1));
            }
        }

        /// <summary>
        /// Just to be extra command-line-friendly, allow the user to indicate true with various synonyms.
        /// </summary>
        private static string[] Trues = { bool.TrueString.ToLower(), "t", "yes", "y" };

        /// <summary>
        /// Just to be extra command-line-friendly, allow the user to indicate false with various synonyms.
        /// </summary>
        private static string[] Falses = { bool.FalseString.ToLower(), "f", "no", "n" };

        /// <summary>
        /// Set the value of this instance's field.
        /// Appropriate type conversion is performed.
        ///
        /// If you are using custom types for your settings, you'll want to
        /// create a class that extends TypeConverter, or modify this method
        /// to handle it specially. (TypeConverter is preferred!)
        /// See https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.typeconverter
        /// </summary>
        /// <param name="field"></param>
        /// <param name="rawValue"></param>
        /// <returns></returns>
        private ErrorCode? SetField(FieldInfo field, string rawValue)
        {
            switch (field.FieldType.FullName)
            {
                case "System.String":
                    field.SetValue(this, rawValue ?? "");
                    return null;
                case "System.Boolean":
                    {
                        rawValue = (rawValue ?? bool.TrueString).Trim().ToLower();
                        if (Trues.Contains(rawValue))
                        {
                            field.SetValue(this, true);
                            return null;
                        }
                        if (Falses.Contains(rawValue))
                        {
                            field.SetValue(this, false);
                            return null;
                        }
                        return ErrorCode.BadValue;
                    }
                default:
                    // If there exists a TypeConverter for this type, use that.
                    object typeValue = Convert(rawValue, field.FieldType);
                    if (typeValue != null)
                    {
                        field.SetValue(this, typeValue);
                        return null;
                    }
                    throw new Exception($"There's no converter or case to handle field \"{field.Name}\" type {field.FieldType.FullName}");
            }
        }

        /// <summary>
        /// Apply a type converter to the raw value string to get the
        /// correct type value.
        /// </summary>
        /// <param name="raw">Raw value text.</param>
        /// <param name="t">Desired type.</param>
        /// <returns>The typed value, or null if:
        /// no type converter, or conversion failed.</returns>
        private static object Convert(string raw, Type t)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(t);
                if (converter != null)
                {
                    return converter.ConvertFromString(raw);
                }
                return null;
            }
            catch (NotSupportedException)
            {
                return null;
            }
        }

    }

    /// <summary>
    /// Common settings used by many kinds of command line tools.
    /// </summary>
    public class StandardCommandLine : CommandLine
    {
        [Setting(Usage = "Enable verbose output")]
        public bool Verbose = false;

        [Setting(Usage = "List possible settings", Special = true)]
        public bool Help = false;

        public StandardCommandLine(StringComparer keyComparer = null) : base(keyComparer: keyComparer) { }

        override public Status PostProcess()
        {
            if (this.Help)
            {
                Usage();
                return Status.HELP;
            }
            else
            {
                return base.PostProcess();
            }
        }
    }
}
