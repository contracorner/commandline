﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CCToolbox
{
    public enum ErrorCode
    {
        Unknown,
        BadValue,
        MissingValue,
        RequiredSettings,
        Processed,
        // If you add new codes, update the Report() extension as well.
    }

    public static class ErrorCodeExtensions
    {
        public static void Report(this ErrorCode errorCode, string arg)
        {
            switch (errorCode)
            {
                case ErrorCode.Unknown:
                    Console.Error.WriteLine($"Unknown command line setting: \"{arg}\"");
                    break;
                case ErrorCode.BadValue:
                    Console.Error.WriteLine($"Bad value: \"{arg}\"");
                    break;
                case ErrorCode.MissingValue:
                    Console.Error.WriteLine($"Missing value: \"{arg}\"");
                    break;
                case ErrorCode.RequiredSettings:
                    Console.Error.WriteLine($"Required settings are missing: {arg}");
                    break;
                case ErrorCode.Processed:
                    Console.Error.WriteLine("Command line processing can only be performed once.");
                    break;
                default:
                    Console.Error.WriteLine($"Unknown error: {errorCode} for \"{arg}\"");
                    break;
            }
        }

    }
}
