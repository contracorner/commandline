﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CCToolbox
{
    /// <summary>
    /// Command line fields that are settings should be decorated with a [Setting] attribute.
    /// The attribute name will be used as the setting's name on the command line, and the value
    /// will be parsed from the raw text string into the property's type.
    /// 
    /// You can add properties to the attribute to indicate additional information.
    ///
    /// NOTE: You can only apply this to _instance_ _field_ properties.
    /// You cannot apply it to computed properties or static fields.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class SettingAttribute : System.Attribute
    {
         /// <summary>
        /// A terse, one-liner to explain this setting's purpose or usage.
        /// This property is optional.
        /// </summary>
        public string Usage = null;

       /// <summary>
        /// If true, then the setting must appear on the command line.
        /// The property's initial value will be overridden in that case so there's no need to provide an initial value
        /// If false, then the setting's initial value will be the default value.
        /// </summary>
        public bool Required = false;

        /// <summary>
        /// If a Special setting is used, then it ignores the Required feature of all other settings.
        /// This is useful for special actions like "/version" or "/help".
        /// </summary>
        public bool Special = false;
    }
}
